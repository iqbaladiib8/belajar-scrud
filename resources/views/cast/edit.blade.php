@extends('layout.master')

@section('judul')
Edit Cast {{$cast->nama}}
@endsection

@section('content')


<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label >Nama</label>
        <input type="text" class="form-control" name="nama" value="{{$cast->nama}}"  placeholder="Masukkan Nama">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label >Umur</label>
        <input type="number" class="form-control" name="umur" value="{{$cast->umur}}" placeholder="Masukkan Umur">
        @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Bio</label>
        <textarea name="bio" class="form-control" cols="30" rows="10" placeholder="Masukkan Bio">{{$cast->bio}}</textarea>
        @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Edit</button>
</form>

@endsection